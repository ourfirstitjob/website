const sequelize = require('sequelize');
const { DataTypes } = require('sequelize');

module.exports = (sequelize ) =>{
    const Member = sequelize.define('member', {
        firstName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        lastName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        linkedinLink: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        githubLink: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        profilePhoto: {
            type: DataTypes.STRING,
            allowNull: false,
        },

    }) 
}