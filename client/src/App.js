import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import NavBar from "./components/NavBar"
import Home from "./components/Home"

function App() {
  return (
    <div>
      <Router>
        <NavBar/>
        <Switch>
          <Route exact path="/" render={()=> <Home/>}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
