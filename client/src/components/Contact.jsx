import React from 'react';
import '../css/contact.css';
import mailImg from '../img/mail-imagen.png';

const Contact = () => {
    return (
        <div id="formularioUnirse" className="text-center seccion-formulario">
            <div className="container">
                <h1>Sumate a la comunidad</h1>
                <div className="row-cols-2 row contenedor-formulario">
                    <div className="col texto-formulario">
                        <p>Te invitamos a que nos dejes tus datos de contacto,  nos cuentes un poco acerca de tu perfil y en qué área de IT te especializas. ¡Esperamos conocerte pronto!</p>
                    </div>
                    <div className="col imagen-mail">
                        <img src={mailImg} alt="mandar-email" />
                    </div>
                </div>
                <div className="container-btn-unirse">
                    <a href="mailto:info@ourfirstitjob.com" type="button" className="btn button-cono btn-primary btn-lg px-4 me-sm-3">                        
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-envelope-fill" viewBox="0 0 16 16">
                            <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555zM0 4.697v7.104l5.803-3.558L0 4.697zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757zm3.436-.586L16 11.801V4.697l-5.803 3.546z"/>
                          </svg>
                        Escribinos
                    </a>

                    <a href="https://ourfirstitjob.slack.com/ssb/redirect" className="btn button-cono btn-primary btn-lg px-4 me-sm-3">                        
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-slack" viewBox="0 0 16 16">
                            <path d="M3.362 10.11c0 .926-.756 1.681-1.681 1.681S0 11.036 0 10.111C0 9.186.756 8.43 1.68 8.43h1.682v1.68zm.846 0c0-.924.756-1.68 1.681-1.68s1.681.756 1.681 1.68v4.21c0 .924-.756 1.68-1.68 1.68a1.685 1.685 0 0 1-1.682-1.68v-4.21zM5.89 3.362c-.926 0-1.682-.756-1.682-1.681S4.964 0 5.89 0s1.68.756 1.68 1.68v1.682H5.89zm0 .846c.924 0 1.68.756 1.68 1.681S6.814 7.57 5.89 7.57H1.68C.757 7.57 0 6.814 0 5.89c0-.926.756-1.682 1.68-1.682h4.21zm6.749 1.682c0-.926.755-1.682 1.68-1.682.925 0 1.681.756 1.681 1.681s-.756 1.681-1.68 1.681h-1.681V5.89zm-.848 0c0 .924-.755 1.68-1.68 1.68A1.685 1.685 0 0 1 8.43 5.89V1.68C8.43.757 9.186 0 10.11 0c.926 0 1.681.756 1.681 1.68v4.21zm-1.681 6.748c.926 0 1.682.756 1.682 1.681S11.036 16 10.11 16s-1.681-.756-1.681-1.68v-1.682h1.68zm0-.847c-.924 0-1.68-.755-1.68-1.68 0-.925.756-1.681 1.68-1.681h4.21c.924 0 1.68.756 1.68 1.68 0 .926-.756 1.681-1.68 1.681h-4.21z"></path>
                          </svg>
                        Unite al Slack
                    </a>
                </div>

            </div>

        </div>
    );
}

export default Contact;
