import React from 'react'
import "../css/welcome.css"
import team from "../img/team.jpg"

function Welcome() {
    return (
        <div className="seccion-comunidad">

            <div className="text-center comunidad-titulo">
                <h1>Bienvenidx a OurFirstITJob</h1>
            </div>

            <div className="container">
                <div className="row comunidad-contenedor">
                    <div className="col-6 comunidad-descripciones">
                        <div className="comunidad-descripcion">
                            <h2 className="comunidad-subtitulo h3"><strong>¿Quiénes Somos?</strong></h2>
                            <p className="text-justify">
                            Somos una comunidad sin fines de lucro, conformada por jóvenes que estamos dando nuestros primeros pasos en el mundo de IT. Nuestro objetivo principal es ayudarnos a conseguir nuestra primera experiencia laboral en el área.
                            </p>
                        </div>
                        <div className="comunidad-descripcion">
                            <h2 className="comunidad-subtitulo h3"><strong>Lo que nos motiva</strong></h2>
                            <p className="text-justify">
                            <ul>
                                <li>Aprendizaje integral de diferentes tecnologías</li>
                                <li>Crear una red colaborativa en torno al desarrollo de nuestras habilidades.</li>
                                <li>Prepararnos para la inserción laboral</li>
                            </ul>
                            </p>
                        </div>  
                        <div className="comunidad-descripcion">
                            <h2 className="comunidad-subtitulo h3"><strong>¿Qué hacemos?</strong></h2>
                            <p className="text-justify">
                            Realizamos proyectos de código abierto y actividades que buscan simular un entorno laboral, mediante el trabajo día a día en equipo, el uso de metodologías ágiles y capacitaciones que nos permiten potenciar nuestrxs skills y prepararnos para las diferentes áreas del mundo de IT.
                            </p>
                        </div>

                        {/* <a className="comunidad-btn" href="#">
                            <button className="btn button-cono btn-primary btn-lg px-4 me-sm-3">Conócenos</button>
                        </a>  */}
                    </div>


                    <div className="col-6 comunidad-imagen">
                        <img className="" src={team}  alt="equipo" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Welcome
