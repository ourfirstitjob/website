import React from 'react'
import '../css/projects.css'
import ayutec from '../img/ayutec.png'
import ayutecMob from '../img/ayutec-mob.png'
import website from '../img/website.png'
import websiteResp from '../img/website-resp.png'
import websiteScrum from '../img/website-scrum.jpg'

export default function Projects() {
    return (
        <div>
            <div className="container">
            <div className="proyectos-titulo row">
                <h1 className="">Un espacio para desarrollar<br/>proyectos colaborativos</h1>
            </div>

            <div className="proySection row">
                <div className="col-6">
                    <img className="d-none d-md-block img-fluid" alt="" src={ayutec} />
                    <img className="d-sm-block d-md-none img-fluid" alt="" src={ayutecMob} />
                </div>
                <div className="proyecto1-texto col-6">
                    <h4 className="proyecto1-titulo"><strong>Ayutec - App Web</strong></h4>
                    <p>Aplicación web de ayuda al servicio técnico, en la cual se puede gestionar y digitalizar el trabajo de los clientes. Proyecto realizado con NodeJs, Express y Bootstrap  . Se diseñó el prototipo con Adobe XD.
                    </p>
                </div>
            </div>
            <div className="proySection row">
                <div className="proyecto1-texto col-6">
                    <h4 className="proyecto1-titulo"><strong>OurFirstITJob - Proyecto de Capacitación.</strong></h4>
                    <p>Estamos realizando una capacitación en Scrum. Es de cuatro encuentros en total. Ya iniciamos y
                        vamos por el segundo.
                    </p>
                </div>
                <div className="col-6">
                    <img className="img-fluid" alt="" src={websiteScrum}></img>
                </div>
            </div>

             {/* <div className="proySection row">
                <div className="proyecto1-texto col-6">
                    <h4 className="proyecto1-titulo"><strong>OurFirstITJOb - Proyecto de Capacitación.</strong></h4>
                    <p>Aplicación web para la comunidad. Proyecto realizado con tecnologías HTML, CSS y Bootstrap.
                    </p>
                </div>
                <div className="col-6">
                    <img className="img-fluid" src="img/website-scrum.jpg" />
                </div>
            </div> */}
            <div className="proySection row">
                <div className="col-6">
                    <img className="d-none d-md-block img-fluid" alt="" src={website} />
                    <img className="d-sm-block d-md-none img-fluid" alt="" src={websiteResp} />
                </div>   
                <div className="proyecto2-texto col-6">
                    <h4 className="proyecto2-titulo"><strong>OurFirstITJob - Proyecto Website de la comunidad</strong></h4>
                    <p>Aplicación web para la comunidad. Proyecto realizado con tecnologías HTML, CSS y Bootstrap.</p>
                </div>
                            
            </div>

        </div>

        </div>
    )
}
