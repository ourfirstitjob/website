import "../css/home.css"
import Welcome from "./Welcome"
import Projects from "./Projects"
import Contact from "./Contact"

export default function Home(){

    return(
    <div>
        <div className="hero px-4 py-5 my-5 text-center">
                <div className="background-overlay">
                    <h1 className="display-5 fw-bold">Bienvenidxs</h1>
                    <div className="col-lg-6 mx-auto">
                        <p className="lead mb-4">Somos una comunidad que facilita primeras experiencias a personas que están
                            intentando insertarse en el mundo laboral IT. ¡Súmate a nuestro equipo!
                        </p>
                        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                            <a href="#formularioUnirse" type="button"
                                className="btn button-cta btn-primary btn-lg px-4 me-sm-3">Quiero unirme</a>
                        </div>
                    </div>
                </div>
            </div>
            <Welcome/>
            <Projects/>
            <Contact/>
    </div>)
}