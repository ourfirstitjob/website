import logo from "../img/logo/logoblanco.png"
import "../css/navbar.css"

export default function NavBar(){
    return(
            <div className="navbar">
                    <img src={logo} height="65" alt="Logo de Our First IT Job" className="logo-img"/>
            </div>
        )
}